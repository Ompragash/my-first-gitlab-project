# Install required packages and python modules
sudo dnf install git python3.9 podman -y
pip3 install ansible-builder ansible-runner

# CLone this repo
git clone https://gitlab.com/Ompragash/awx-ee-custom.git
cd awx-ee-custom/

# Run ansible-builder to build a ee image
ansible-builder build --help
ansible-builder build -t registry.gitlab.com/ompragash/awx-ee-custom -v 3
podman images

# Login to gitlab registry and push the newly built image using `ansible-builder`
podman login registry.gitlab.com
podman push registry.gitlab.com/ompragash/awx-ee-custom
